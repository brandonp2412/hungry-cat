namespace hungry_cat.Models
{
  public class ApplicationUserCat
  {
    public string ApplicationUserId { get; set; }

    public ApplicationUser ApplicationUser { get; set; }

    public long CatId { get; set; }

    public Cat Cat { get; set; }

    public override string ToString()
    {
      return $"ApplicationUserId={this.ApplicationUserId},CatId={this.CatId}";
    }
  }
}