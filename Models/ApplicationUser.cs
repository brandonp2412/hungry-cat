﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hungry_cat.Models
{
  public class ApplicationUser : IdentityUser
  {
    public virtual ICollection<ApplicationUserCat> ApplicationUserCats { get; set; }

    public virtual ICollection<ShareCatRequest> ShareCatRequests { get; set; }
  }
}
