using System.Reflection;
using System.Text;

namespace hungry_cat.Models
{
  public class ShareCatRequest
  {
    public string RequesteeId { get; set; }

    public ApplicationUser Requestee { get; set; }

    public string RequestorId { get; set; }

    public ApplicationUser Requestor { get; set; }

    public long CatId { get; set; }

    public Cat Cat { get; set; }

    private PropertyInfo[] _PropertyInfos = null;

    public override string ToString()
    {
      if (_PropertyInfos == null)
        _PropertyInfos = this.GetType().GetProperties();

      var sb = new StringBuilder();

      foreach (var info in _PropertyInfos)
      {
        var value = info.GetValue(this, null) ?? "(null)";
        sb.AppendLine(info.Name + ": " + value.ToString());
      }

      return sb.ToString();
    }
  }
}