using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Reflection;
using System.Text;

namespace hungry_cat.Models
{
  public class Cat
  {
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public long Id { get; set; }

    public string Name { get; set; }

    public bool Hungry { get; set; }

    public string Image { get; set; }

    public TimeSpan Breakfast { get; set; }

    public TimeSpan Dinner { get; set; }

    public virtual ICollection<ApplicationUserCat> ApplicationUserCats { get; set; }

    public virtual ICollection<ShareCatRequest> ShareCatRequests { get; set; }

    public override string ToString()
    {
      return $"Id={this.Id}," +
        $"Name={this.Name}," +
        $"Hungry={this.Hungry}," +
        $"Breakfast={this.Breakfast}," +
        $"Dinner={this.Dinner}";
    }
  }
}