#!/usr/bin/env bash

set -ex
(
  cd ClientApp
  npm i
  npm run build
)

dotnet publish -c Release -o ~/hungry-cat
cd ~/hungry-cat
pid_location="$HOME/hungry-cat/pid"
pid=$(cat "$pid_location")
kill "$pid" || true
dotnet hungry-cat.dll &>> /var/log/hungry-cat.log &
echo $! > ~/hungry-cat/pid