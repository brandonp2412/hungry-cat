﻿using hungry_cat.Models;
using IdentityServer4.EntityFramework.Options;
using Microsoft.AspNetCore.ApiAuthorization.IdentityServer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hungry_cat.Data
{
  public class ApplicationDbContext : ApiAuthorizationDbContext<ApplicationUser>
  {
    public ApplicationDbContext(
        DbContextOptions options,
        IOptions<OperationalStoreOptions> operationalStoreOptions) : base(options, operationalStoreOptions)
    {
    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
      base.OnModelCreating(builder);

      builder.Entity<ApplicationUserCat>()
        .HasKey(a => new { a.CatId, a.ApplicationUserId });
      builder.Entity<ApplicationUserCat>()
        .HasOne(a => a.Cat)
        .WithMany(a => a.ApplicationUserCats)
        .HasForeignKey(a => a.CatId);
      builder.Entity<ApplicationUserCat>()
        .HasOne(a => a.ApplicationUser)
        .WithMany(a => a.ApplicationUserCats)
        .HasForeignKey(a => a.ApplicationUserId);

      builder.Entity<ShareCatRequest>()
        .HasKey(scr => new { scr.CatId, scr.RequesteeId });
      builder.Entity<ShareCatRequest>()
        .HasOne(a => a.Cat)
        .WithMany(a => a.ShareCatRequests)
        .HasForeignKey(a => a.CatId);
      builder.Entity<ShareCatRequest>()
        .HasOne(a => a.Requestee)
        .WithMany(a => a.ShareCatRequests)
        .HasForeignKey(a => a.RequesteeId);
    }


    public DbSet<Cat> Cats { get; set; }

    public DbSet<ApplicationUser> AspNetUsers { get; set; }

    public DbSet<ApplicationUserCat> ApplicationUserCat { get; set; }
    public DbSet<ShareCatRequest> ShareCatRequests { get; internal set; }
  }
}
