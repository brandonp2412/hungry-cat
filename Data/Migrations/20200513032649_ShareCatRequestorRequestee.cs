﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace hungry_cat.Data.Migrations
{
    public partial class ShareCatRequestorRequestee : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ShareCatRequests_AspNetUsers_ApplicationUserId",
                table: "ShareCatRequests");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ShareCatRequests",
                table: "ShareCatRequests");

            migrationBuilder.DropIndex(
                name: "IX_ShareCatRequests_ApplicationUserId",
                table: "ShareCatRequests");

            migrationBuilder.DropColumn(
                name: "ApplicationUserId",
                table: "ShareCatRequests");

            migrationBuilder.AddColumn<string>(
                name: "RequesteeId",
                table: "ShareCatRequests",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "RequestorId",
                table: "ShareCatRequests",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_ShareCatRequests",
                table: "ShareCatRequests",
                columns: new[] { "CatId", "RequesteeId" });

            migrationBuilder.CreateIndex(
                name: "IX_ShareCatRequests_RequesteeId",
                table: "ShareCatRequests",
                column: "RequesteeId");

            migrationBuilder.CreateIndex(
                name: "IX_ShareCatRequests_RequestorId",
                table: "ShareCatRequests",
                column: "RequestorId");

            migrationBuilder.AddForeignKey(
                name: "FK_ShareCatRequests_AspNetUsers_RequesteeId",
                table: "ShareCatRequests",
                column: "RequesteeId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ShareCatRequests_AspNetUsers_RequestorId",
                table: "ShareCatRequests",
                column: "RequestorId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ShareCatRequests_AspNetUsers_RequesteeId",
                table: "ShareCatRequests");

            migrationBuilder.DropForeignKey(
                name: "FK_ShareCatRequests_AspNetUsers_RequestorId",
                table: "ShareCatRequests");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ShareCatRequests",
                table: "ShareCatRequests");

            migrationBuilder.DropIndex(
                name: "IX_ShareCatRequests_RequesteeId",
                table: "ShareCatRequests");

            migrationBuilder.DropIndex(
                name: "IX_ShareCatRequests_RequestorId",
                table: "ShareCatRequests");

            migrationBuilder.DropColumn(
                name: "RequesteeId",
                table: "ShareCatRequests");

            migrationBuilder.DropColumn(
                name: "RequestorId",
                table: "ShareCatRequests");

            migrationBuilder.AddColumn<string>(
                name: "ApplicationUserId",
                table: "ShareCatRequests",
                type: "nvarchar(450)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ShareCatRequests",
                table: "ShareCatRequests",
                columns: new[] { "CatId", "ApplicationUserId" });

            migrationBuilder.CreateIndex(
                name: "IX_ShareCatRequests_ApplicationUserId",
                table: "ShareCatRequests",
                column: "ApplicationUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_ShareCatRequests_AspNetUsers_ApplicationUserId",
                table: "ShareCatRequests",
                column: "ApplicationUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
