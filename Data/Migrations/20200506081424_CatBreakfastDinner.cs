﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace hungry_cat.Data.Migrations
{
    public partial class CatBreakfastDinner : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<TimeSpan>(
                name: "Breakfast",
                table: "Cats",
                nullable: false,
                defaultValue: new TimeSpan(0, 0, 0, 0, 0));

            migrationBuilder.AddColumn<TimeSpan>(
                name: "Dinner",
                table: "Cats",
                nullable: false,
                defaultValue: new TimeSpan(0, 0, 0, 0, 0));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Breakfast",
                table: "Cats");

            migrationBuilder.DropColumn(
                name: "Dinner",
                table: "Cats");
        }
    }
}
