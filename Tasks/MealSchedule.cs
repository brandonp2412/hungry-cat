using System;
using System.Threading;
using System.Threading.Tasks;
using hungry_cat.Data;
using System.Linq;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;

public class MealSchedule : IHostedService
{
  private readonly ILogger<MealSchedule> _logger;

  private readonly IServiceScopeFactory _scopeFactory;

  private Timer _timer;

  public MealSchedule(
    ILogger<MealSchedule> logger,
    IServiceScopeFactory scopeFactory
  )
  {
    _logger = logger;
    _scopeFactory = scopeFactory;
  }

  public Task StartAsync(CancellationToken stoppingToken)
  {
    _logger.LogInformation("Starting...");
    _timer = new Timer(DoWork, null, TimeSpan.Zero, TimeSpan.FromMinutes(1));
    return Task.CompletedTask;
  }

  private void DoWork(object state)
  {
    using (var scope = this._scopeFactory.CreateScope())
    {
      var db = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();
      var limit = 100;
      var total = db.Cats.Count();
      var page = 0;
      while (page * limit < total)
      {
        var cats = db.Cats.Skip(page * limit)
          .Take(limit)
          .ToList();
        foreach (var cat in cats)
        {
          if (this.IsMealTime(cat.Breakfast) || this.IsMealTime(cat.Dinner))
          {
            _logger.LogInformation($"Meal time for '{cat}'.");
            cat.Hungry = true;
            db.SaveChanges();
          }
        }
        page++;
      }
    }
  }

  private bool IsMealTime(TimeSpan timeOfMeal)
  {
    var minutesUntilMeal = (timeOfMeal - DateTime.Now.TimeOfDay)
      .Duration()
      .TotalMinutes;
    return minutesUntilMeal < 1;
  }

  public Task StopAsync(CancellationToken stoppingToken)
  {
    _logger.LogInformation("Stopping...");
    _timer?.Change(Timeout.Infinite, 0);
    return Task.CompletedTask;
  }

  public void Dispose()
  {
    _timer?.Dispose();
  }
}