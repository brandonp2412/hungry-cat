import React from 'react';
import {Route} from 'react-router';
import App from './App';
import {shallow} from 'enzyme';
import AuthorizeRoute from './components/api-authorization/AuthorizeRoute';

describe(App.name, () => {
  it('renders any routes', () => {
    const wrapper = shallow(<App />);
    const route = wrapper.find(Route);
    expect(route.length).toBeGreaterThan(0);
  });

  it('renders any authorized routes', () => {
    const wrapper = shallow(<App />);
    const route = wrapper.find(AuthorizeRoute);
    expect(route.length).toBeGreaterThan(0);
  });
});
