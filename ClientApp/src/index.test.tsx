import ReactDOM from 'react-dom';
jest.mock('react-dom', () => ({
  render: jest.fn(),
}));

describe('Index', () => {
  it('should call react dom render', () => {
    document.getElementsByTagName = () =>
      [{getAttribute: () => '', hasAttribute: () => true}] as any;
    require('./index');
    expect(ReactDOM.render).toHaveBeenCalled();
  });
});
