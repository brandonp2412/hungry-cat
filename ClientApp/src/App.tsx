import React, {Component} from 'react';
import {Route} from 'react-router';
import {Layout} from './components/Layout';
import {HomePage} from './components/HomePage';
import {CatEdit} from './components/cat/CatEdit';
import AuthorizeRoute from './components/api-authorization/AuthorizeRoute';
import ApiAuthorizationRoutes from './components/api-authorization/ApiAuthorizationRoutes';
import {ApplicationPaths} from './components/api-authorization/ApiAuthorizationConstants';
import './custom.css';
import {SharePage} from './components/share/SharePage';
import {ShareEdit} from './components/share/ShareEdit';

export default class App extends Component {
  static displayName = App.name;

  render() {
    return (
      <Layout>
        <Route exact={true} path='/' component={HomePage} />
        <AuthorizeRoute path='/cat/:id' component={CatEdit} />
        <AuthorizeRoute path='/share' component={SharePage} />
        <AuthorizeRoute path='/share-request/:id' component={ShareEdit} />
        <Route
          path={ApplicationPaths.ApiAuthorizationPrefix}
          component={ApiAuthorizationRoutes}
        />
        <div className='footer'>
          <a
            href='https://gitlab.brandon.nz/brandon/hungry-cat'
            target='_blank'
            rel='noopener noreferrer'
          >
            <img src='gitlab.svg' alt='GitLab' />
            Source code
          </a>
        </div>
      </Layout>
    );
  }
}
