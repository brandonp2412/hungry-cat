import {FileService} from './FileService';

describe(FileService.name, () => {
  it('should convert', async () => {
    const file = new File([], '');
    const base64 = await FileService.convert(file);
    expect(base64).toBeTruthy();
  });
});
