import React, {Component} from 'react';
import authService from './api-authorization/AuthorizeService';
import {NavLink} from 'react-router-dom';
import {Button, Spinner} from 'reactstrap';
import {Cat} from './cat/Cat.model';
import {CatTable} from './cat/CatTable';
import {faPlus, faSync} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {CatService} from './cat/CatService';

export class HomePage extends Component<
  {},
  {
    cats: Cat[];
    loadingCats: boolean;
    authenticated: boolean;
    loadingUser: boolean;
  }
> {
  static displayName = HomePage.name;

  constructor(props: {}) {
    super(props);
    this.state = {
      cats: [],
      loadingCats: false,
      authenticated: false,
      loadingUser: false,
    };
    this.getCats = this.getCats.bind(this);
  }

  async componentDidMount() {
    try {
      this.setState({loadingUser: true});
      if (!(await authService.isAuthenticated())) return;
      this.setState({authenticated: true});
      this.getCats();
    } finally {
      this.setState({loadingUser: false});
    }
  }

  async getCats() {
    try {
      this.setState({loadingCats: true});
      this.setState({
        cats: await CatService.get(),
      });
    } finally {
      this.setState({loadingCats: false});
    }
  }

  static renderUnauthenticated() {
    return (
      <div id='unauthenticated'>
        Looks like you haven't{' '}
        <NavLink to='/authentication/login' className='a'>
          logged in
        </NavLink>{' '}
        yet. You need to be logged in with an account to see your kitties.
      </div>
    );
  }

  render() {
    return (
      <div>
        <h1 className='d-inline'>Meet your Cats</h1>
        {this.state.authenticated && (
          <div className='float-right'>
            <Button
              className='mr-2'
              outline={true}
              color='info'
              onClick={this.getCats}
            >
              <FontAwesomeIcon icon={faSync} />
            </Button>
            <NavLink className='text-dark' to='/cat/new'>
              <Button color='primary'>
                <FontAwesomeIcon icon={faPlus} />
                Add Cat
              </Button>
            </NavLink>
          </div>
        )}
        <p>Here you can manage your cats diets.</p>
        {this.state.loadingUser && <Spinner />}
        {this.state.authenticated && (
          <CatTable
            cats={this.state.cats}
            onChange={this.getCats}
            loading={this.state.loadingCats}
          />
        )}
        {!this.state.authenticated &&
          !this.state.loadingUser &&
          HomePage.renderUnauthenticated()}
      </div>
    );
  }
}
