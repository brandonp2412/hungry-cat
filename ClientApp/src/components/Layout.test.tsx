import React from 'react';
import {shallow} from 'enzyme';
import {Layout} from './Layout';
import {NavMenu} from './NavMenu';
import {Container} from 'reactstrap';

describe(Layout.name, () => {
  it('should render a nav menu', () => {
    const wrapper = shallow<Layout>(<Layout />);
    const nav = wrapper.find(NavMenu);
    expect(nav.length).toEqual(1);
  });

  it('should render inner content', () => {
    const wrapper = shallow<Layout>(<Layout>Hello, world!</Layout>);
    const container = wrapper.find(Container);
    expect(container.childAt(0).text()).toEqual('Hello, world!');
  });
});
