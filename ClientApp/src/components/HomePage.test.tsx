import React from 'react';
import {shallow} from 'enzyme';
import {HomePage} from './HomePage';
import authService from './api-authorization/AuthorizeService';
import {Spinner} from 'reactstrap';
import {CatService} from './cat/CatService';

describe(HomePage.name, () => {
  it('renders loading', () => {
    const wrapper = shallow<HomePage>(<HomePage />);
    wrapper.instance().setState({loadingCats: true});
    const loading = wrapper.find(Spinner);
    expect(loading.exists()).toBeTruthy();
  });

  it('renders title', () => {
    const wrapper = shallow<HomePage>(<HomePage />);
    const title = wrapper.find('h1');
    expect(title.text()).toEqual('Meet your Cats');
  });

  it('renders unauthenticated text', () => {
    const wrapper = shallow<HomePage>(<HomePage />);
    wrapper.instance().setState({loadingUser: false, authenticated: false});
    const paragraph = wrapper.find('div#unauthenticated');
    expect(paragraph.text()).toContain('You need to be logged in');
  });

  it('mounts component', async () => {
    const wrapper = shallow<HomePage>(<HomePage />);
    const instance = wrapper.instance();
    authService.isAuthenticated = () => Promise.resolve(true);
    CatService.get = () => Promise.resolve([]);
    await instance.componentDidMount();
    expect(instance.state.loadingCats).toBeFalsy();
    expect(instance.state.cats).toEqual([]);
  });

  it('gets cats', async () => {
    const wrapper = shallow<HomePage>(<HomePage />);
    const instance = wrapper.instance();
    const cats = [{id: 1, name: 'Cat', hungry: false}];
    CatService.get = () => Promise.resolve(cats);
    await instance.getCats();
    expect(instance.state.cats).toEqual(cats);
  });
});
