import React from 'react';
import {NavMenu} from './NavMenu';
import {shallow} from 'enzyme';
import {NavbarBrand} from 'reactstrap';

describe(NavMenu.name, () => {
  const wrapper = shallow<NavMenu>(<NavMenu />);

  it('renders navbar brand', () => {
    const nav = wrapper.find(NavbarBrand).childAt(0).text();
    expect(nav).toEqual('Hungry Cat');
  });

  it('renders home link(s)', () => {
    const link = wrapper.find({to: '/'});
    expect(link.length).toBeGreaterThan(0);
  });

  it('renders API link', () => {
    const link = wrapper.find({to: '/swagger'});
    expect(link.length).toBeGreaterThan(0);
  });

  it('should toggle navbar', () => {
    const instance = wrapper.instance();
    const collapsed = instance.state.collapsed;
    instance.toggleNavbar();
    expect(collapsed).toEqual(!instance.state.collapsed);
  });
});
