import React from 'react';
import AuthorizeRoute from './AuthorizeRoute';
import {shallow} from 'enzyme';
import {CatEdit} from '../cat/CatEdit';
import authService from './AuthorizeService';
import {Redirect} from 'react-router-dom';

describe(AuthorizeRoute.name, () => {
  const auth = shallow<AuthorizeRoute>(
    <AuthorizeRoute path='add-cat' component={CatEdit} />,
  );

  it('should mount', () => {
    authService.subscribe = jest.fn((callback) => {
      callback();
      return 1;
    });
    auth.instance().componentDidMount();
    expect(authService.subscribe).toHaveBeenCalled();
  });

  it('should unmount', () => {
    authService.unsubscribe = jest.fn();
    auth.instance().componentWillUnmount();
    expect(authService.unsubscribe).toHaveBeenCalled();
  });

  it('should render route authenticated', () => {
    const instance = auth.instance();
    instance.setState({authenticated: true});
    const result = instance.renderRoute({});
    // @ts-ignore
    expect(result).toEqual(<CatEdit />);
  });

  it('should render route unauthenticated', () => {
    const instance = auth.instance();
    instance.setState({authenticated: false});
    const result = instance.renderRoute({});
    expect(result).toEqual(
      <Redirect to='/authentication/login?returnUrl=http://localhost/add-cat' />,
    );
  });
});
