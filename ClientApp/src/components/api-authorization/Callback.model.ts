export interface Callback {
  subscription: number;
  callback: () => {};
}
