import React from 'react';
import {Login} from './Login';
import {shallow} from 'enzyme';
import {LoginActions} from './ApiAuthorizationConstants';
import authService, {AuthenticationResultStatus} from './AuthorizeService';

window.location.replace = () => true;

describe(Login.name, () => {
  const wrapper = shallow<Login>(<Login action='login' />);

  it('should complete all login actions', () => {
    wrapper.setProps({action: LoginActions.Login});
    wrapper.instance().componentDidMount();
    wrapper.setProps({action: LoginActions.LoginCallback});
    wrapper.instance().componentDidMount();
    wrapper.setProps({action: LoginActions.Profile});
    wrapper.instance().componentDidMount();
    wrapper.setProps({action: LoginActions.Register});
    wrapper.instance().componentDidMount();
  });

  it('should login with any status', () => {
    const instance = wrapper.instance();
    authService.signIn = jest.fn(() =>
      Promise.resolve({status: AuthenticationResultStatus.Redirect}),
    );
    instance.login('');
    authService.signIn = jest.fn(() =>
      Promise.resolve({status: AuthenticationResultStatus.Success}),
    );
    instance.login('');
    authService.signIn = jest.fn(() =>
      Promise.resolve({status: AuthenticationResultStatus.Fail}),
    );
    instance.login('');
  });

  it('should process login callback', () => {
    authService.completeSignIn = () =>
      Promise.resolve({status: AuthenticationResultStatus.Success});
    wrapper.instance().processLoginCallback();
    authService.completeSignIn = () =>
      Promise.resolve({status: AuthenticationResultStatus.Fail});
    wrapper.instance().processLoginCallback();
  });

  it('should get return url', () => {
    wrapper.instance().getReturnUrl();
  });

  it('should redirect to register', () => {
    wrapper.instance().redirectToRegister();
  });

  it('should redirect to profile', () => {
    wrapper.instance().redirectToProfile();
  });

  it('should redirect to api authorization path', () => {
    wrapper.instance().redirectToApiAuthorizationPath('');
  });

  it('should navigate to return url', () => {
    wrapper.instance().navigateToReturnUrl('');
  });
});
