import authService, {AuthorizeService} from './AuthorizeService';
import {User} from './User.model';

describe(AuthorizeService.name, () => {
  it('should ensure user manager is initialized', async () => {
    await authService.ensureUserManagerInitialized();
    expect(authService.userManager).toBeTruthy();
  });

  it('should redirect', () => {
    authService.redirect();
  });

  it('should succeed', () => {
    authService.success({returnUrl: 'test'});
  });

  it('should error', () => {
    authService.error('some message');
  });

  it('should create arguments', () => {
    authService.createArguments({returnUrl: 'test'});
  });

  it('should notify subscribers', () => {
    authService.notifySubscribers();
  });

  it('should subscribe', () => {
    const id = authService.subscribe(() => true);
    authService.unsubscribe(id);
  });

  it('should update state', () => {
    authService.updateState({} as User);
  });

  it('should complete sign out', () => {
    authService.completeSignOut('hello-url');
  });

  it('should fail to sign out with popup disabled', () => {
    authService.signOut({returnUrl: 'nope'});
  });

  it('should sign out', () => {
    authService._popUpDisabled = false;
    authService.userManager.signoutPopup = () => Promise.resolve();
    authService.signOut({returnUrl: 'nope'});
  });

  it('should complete sign in', () => {
    authService.userManager.signinCallback = () => Promise.resolve();
    authService.completeSignIn('hello2-url');
  });

  it('should sign in', () => {
    authService.userManager.signinSilent = () => Promise.resolve({});
    authService.signIn({returnUrl: 'test'});
  });

  it('should catch silent errors', () => {
    authService.userManager.signinSilent = () => {
      throw new Error();
    };
    authService._popUpDisabled = true;
    authService.signIn({returnUrl: 'test'});
    authService._popUpDisabled = false;
    authService.userManager.signinPopup = () => Promise.resolve({});
    authService.signIn({returnUrl: 'test'});
  });

  it('should catch signin popup errors', () => {
    authService.userManager.signinSilent = () => {
      throw new Error();
    };
    authService._popUpDisabled = true;
    authService.userManager.signinPopup = () => {
      throw new Error();
    };
    authService.signIn({returnUrl: 'test'});
  });

  it('should catch signin popup redirect errors', () => {
    authService.userManager.signinSilent = () => {
      throw new Error();
    };
    authService._popUpDisabled = true;
    authService.userManager.signinPopup = () => {
      throw new Error();
    };
    authService.userManager.signinRedirect = () => {
      throw new Error();
    };
    authService.signIn({returnUrl: 'test'});
  });

  it('should get access token', () => {
    const token = authService.getAccessToken();
    expect(token).toBeTruthy();
  });

  it('should get access token', () => {
    const token = authService.getAccessToken();
    expect(token).toBeTruthy();
  });

  it('should get user', () => {
    let user = authService.getUser();
    expect(user).toBeTruthy();
    authService._user = {profile: {}} as User;
    user = authService.getUser();
    expect(user).toBeTruthy();
  });
});
