import React from 'react';
import {Logout} from './Logout';
import {shallow} from 'enzyme';
import {LogoutActions} from './ApiAuthorizationConstants';
import authService, {AuthenticationResultStatus} from './AuthorizeService';

window.location.replace = () => true;

describe(Logout.name, () => {
  it('should render logout action', () => {
    window.history.replaceState({state: {local: true}}, '');
    shallow(<Logout action={LogoutActions.Logout} />);
  });

  it('should render logout callback action', () => {
    window.history.replaceState({state: {local: false}}, '');
    shallow(<Logout action={LogoutActions.LogoutCallback} />);
  });

  it('should render logged out action', () => {
    shallow(<Logout action={LogoutActions.LoggedOut} />);
  });

  it('should logout with redirect', async () => {
    authService.isAuthenticated = () => Promise.resolve(true);
    authService.signOut = () =>
      Promise.resolve({
        status: AuthenticationResultStatus.Redirect,
        message: '',
      });
    const wrapper = shallow<Logout>(
      <Logout action={LogoutActions.LoggedOut} />,
    );
    await wrapper.instance().logout('');
  });

  it('should logout with success', async () => {
    authService.isAuthenticated = () => Promise.resolve(true);
    authService.signOut = () =>
      Promise.resolve({
        status: AuthenticationResultStatus.Success,
        message: '',
      });
    const wrapper = shallow<Logout>(
      <Logout action={LogoutActions.LoggedOut} />,
    );
    await wrapper.instance().logout('');
  });

  it('should logout with failure', async () => {
    authService.isAuthenticated = () => Promise.resolve(true);
    authService.signOut = () =>
      Promise.resolve({
        status: AuthenticationResultStatus.Fail,
        message: '',
      });
    const wrapper = shallow<Logout>(
      <Logout action={LogoutActions.LoggedOut} />,
    );
    await wrapper.instance().logout('');
  });

  it('should process logout callback', async () => {
    authService.completeSignOut = () =>
      Promise.resolve({
        status: AuthenticationResultStatus.Success,
        message: '',
      });
    const wrapper = shallow<Logout>(
      <Logout action={LogoutActions.LoggedOut} />,
    );
    await wrapper.instance().processLogoutCallback();
  });

  it('should process logout callback with failure', async () => {
    authService.completeSignOut = () =>
      Promise.resolve({
        status: AuthenticationResultStatus.Fail,
        message: '',
      });
    const wrapper = shallow<Logout>(
      <Logout action={LogoutActions.LoggedOut} />,
    );
    await wrapper.instance().processLogoutCallback();
  });
});
