import React from 'react';
import {Component} from 'react';
import {Route, Redirect} from 'react-router-dom';
import {
  ApplicationPaths,
  QueryParameterNames,
} from './ApiAuthorizationConstants';
import authService from './AuthorizeService';

export type AuthorizeRouteProps = {
  path: string;
  component: any;
};

export default class AuthorizeRoute extends Component<
  AuthorizeRouteProps,
  {ready: boolean; authenticated: boolean}
> {
  _subscription = 0;

  constructor(props: AuthorizeRouteProps) {
    super(props);

    this.state = {
      ready: false,
      authenticated: false,
    };
    this.renderRoute = this.renderRoute.bind(this);
  }

  componentDidMount() {
    this._subscription = authService.subscribe(() =>
      this.authenticationChanged(),
    );
    this.populateAuthenticationState();
  }

  componentWillUnmount() {
    authService.unsubscribe(this._subscription);
  }

  renderRoute(props: any) {
    const {authenticated} = this.state;
    const link = document.createElement('a');
    link.href = this.props.path;
    const returnUrl = `${link.protocol}//${link.host}${link.pathname}${link.search}${link.hash}`;
    const redirectUrl = `${ApplicationPaths.Login}?${
      QueryParameterNames.ReturnUrl
    }=${encodeURI(returnUrl)}`;
    const {component: AuthorizedComponent} = this.props;
    if (authenticated) {
      return <AuthorizedComponent {...props} />;
    } else {
      return <Redirect to={redirectUrl} />;
    }
  }

  render() {
    const {ready} = this.state;
    const link = document.createElement('a');
    link.href = this.props.path;
    if (!ready) {
      return <div />;
    } else {
      const {component: AuthorizedComponent, ...rest} = this.props;
      return <Route {...rest} render={this.renderRoute} />;
    }
  }

  async populateAuthenticationState() {
    const authenticated = await authService.isAuthenticated();
    this.setState({ready: true, authenticated});
  }

  async authenticationChanged() {
    this.setState({ready: false, authenticated: false});
    await this.populateAuthenticationState();
  }
}
