import ApiAuthorizationRoutes from './ApiAuthorizationRoutes';
import {shallow} from 'enzyme';
import React from 'react';
import {LoginActions, LogoutActions} from './ApiAuthorizationConstants';

describe(ApiAuthorizationRoutes.name, () => {
  const api = shallow<ApiAuthorizationRoutes>(<ApiAuthorizationRoutes />);

  it('should render login action', () => {
    const instance = api.instance();
    expect(instance.loginAction(LoginActions.Login)).toBeTruthy();
  });

  it('should render logout action', () => {
    const instance = api.instance();
    expect(instance.logoutAction(LogoutActions.Logout)).toBeTruthy();
  });
});
