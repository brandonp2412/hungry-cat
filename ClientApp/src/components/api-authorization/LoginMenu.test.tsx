import React from 'react';
import {LoginMenu} from './LoginMenu';
import {shallow} from 'enzyme';

describe(LoginMenu.name, () => {
  const wrapper = shallow<LoginMenu>(<LoginMenu />);

  it('should populate state', () => {
    wrapper.instance().populateState();
  });

  it('should render authenticated', () => {
    wrapper.instance().setState({isAuthenticated: true});
  });

  it('should unmount', () => {
    wrapper.instance().componentWillUnmount();
  });
});
