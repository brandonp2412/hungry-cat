export interface ApplicationUser {
  accessFailedCount: number;
  concurrencyStamp: string;
  email: string;
  emailConfirmed: boolean;
  id: string;
  lockoutEnabled: boolean;
  normalizedEmail: string;
  normalizedUserName: string;
  phoneNumberConfirmed: boolean;
  securityStamp: string;
  twoFactorEnabled: boolean;
  userName: string;
}
