import React, {Component, Fragment} from 'react';
import {Route} from 'react-router';
import {Login} from './Login';
import {Logout} from './Logout';
import {
  ApplicationPaths,
  LoginActions,
  LogoutActions,
} from './ApiAuthorizationConstants';

export default class ApiAuthorizationRoutes extends Component {
  loginAction(name: string) {
    return <Login action={name} />;
  }

  logoutAction(name: string) {
    return <Logout action={name} />;
  }

  render() {
    return (
      <Fragment>
        <Route
          path={ApplicationPaths.Login}
          render={this.loginAction.bind(this, LoginActions.Login)}
        />
        <Route
          path={ApplicationPaths.LoginFailed}
          render={this.loginAction.bind(this, LoginActions.LoginFailed)}
        />
        <Route
          path={ApplicationPaths.LoginCallback}
          render={this.loginAction.bind(this, LoginActions.LoginCallback)}
        />
        <Route
          path={ApplicationPaths.Profile}
          render={this.loginAction.bind(this, LoginActions.Profile)}
        />
        <Route
          path={ApplicationPaths.Register}
          render={this.loginAction.bind(this, LoginActions.Register)}
        />
        <Route
          path={ApplicationPaths.LogOut}
          render={this.logoutAction.bind(this, LogoutActions.Logout)}
        />
        <Route
          path={ApplicationPaths.LogOutCallback}
          render={this.logoutAction.bind(this, LogoutActions.LogoutCallback)}
        />
        <Route
          path={ApplicationPaths.LoggedOut}
          render={this.logoutAction.bind(this, LogoutActions.LoggedOut)}
        />
      </Fragment>
    );
  }
}
