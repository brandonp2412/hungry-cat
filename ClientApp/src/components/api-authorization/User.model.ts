export interface User {
  id_token: string;
  session_state: string;
  access_token: string;
  token_type: string;
  scope: string;
  profile: {
    s_hash: string;
    sid: string;
    sub: string;
    auth_time: number;
    idp: string;
    amr: [string];
    preferred_username: string;
    name: string;
  };
  expires_at: number;
  state: {returnUrl: string};
}
