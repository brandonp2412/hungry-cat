import React, {Component, ChangeEvent, FormEvent} from 'react';
import {
  Button,
  Form,
  Input,
  FormGroup,
  FormText,
  FormFeedback,
  Label,
  Spinner,
} from 'reactstrap';
import {FileService} from '../FileService';
import {RouteComponentProps} from 'react-router-dom';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {
  faSmile,
  faFrown,
  faFile,
  faSave,
} from '@fortawesome/free-solid-svg-icons';
import {Cat} from './Cat.model';
import {CatService} from './CatService';

export type CatEditProps = RouteComponentProps<{id: string}>;

export class CatEdit extends Component<
  CatEditProps,
  {
    cat: Cat;
    invalid: boolean;
    loading: boolean;
    submitting: boolean;
  }
> {
  static displayName = CatEdit.name;

  constructor(props: CatEditProps) {
    super(props);
    this.state = {
      cat: {
        name: '',
        hungry: false,
        breakfast: '07:00',
        dinner: '19:00',
      },
      invalid: true,
      loading: false,
      submitting: false,
    };
    this.handleFileChoose = this.handleFileChoose.bind(this);
    this.handleFile = this.handleFile.bind(this);
    this.handleName = this.handleName.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleBreakfast = this.handleBreakfast.bind(this);
    this.handleDinner = this.handleDinner.bind(this);
  }

  async componentDidMount() {
    const id = Number(this.props.match.params.id);
    if (isNaN(id)) return;
    this.setState({loading: true});
    try {
      const response = await CatService.find(id);
      const cat = await response.json();
      this.setState({cat, invalid: false});
    } finally {
      this.setState({loading: false});
    }
  }

  handleHungry(hungry: boolean) {
    const cat = {...this.state.cat};
    cat.hungry = hungry;
    this.setState({cat});
  }

  handleName(event: ChangeEvent<HTMLInputElement>) {
    const cat = {...this.state.cat};
    cat.name = event.target.value;
    this.setState({cat, invalid: cat.name === ''});
  }

  handleBreakfast(event: ChangeEvent<HTMLInputElement>) {
    const cat = {...this.state.cat};
    cat.breakfast = event.target.value;
    this.setState({cat});
  }

  handleDinner(event: ChangeEvent<HTMLInputElement>) {
    const cat = {...this.state.cat};
    cat.dinner = event.target.value;
    this.setState({cat});
  }

  handleFileChoose() {
    const image = document.getElementById('image');
    if (!image)
      throw new Error('Input element with id="image" does not exist.');
    image.click();
  }

  async handleFile(event: ChangeEvent<HTMLInputElement>) {
    if (!event.target.files || event.target.files.length === 0) return;
    const base64 = await FileService.convert(event.target.files[0]);
    const cat = {...this.state.cat};
    cat.image = base64;
    this.setState({cat});
  }

  async handleSubmit() {
    this.setState({submitting: true});
    try {
      if (this.state.cat.id) {
        await CatService.update(this.state.cat);
      } else {
        await CatService.create(this.state.cat);
      }
    } finally {
      this.setState({submitting: false});
    }
    this.props.history.push('/');
  }

  handlePreventDefault(e: FormEvent<HTMLFormElement>) {
    e.preventDefault();
  }

  renderBody() {
    return (
      <div>
        {this.state.cat.id ? (
          <h1 id='edit-title'>Edit your Cat</h1>
        ) : (
          <h1 id='edit-title'>Add new Cat</h1>
        )}
        <p>
          To {this.state.cat.id ? 'edit your cat' : 'add a new cat'}, complete
          the below form, and then click submit.
        </p>
        <Form onSubmit={this.handlePreventDefault}>
          <FormGroup className='mb-2'>
            <Label for='name'>Name</Label>
            <Input
              type='text'
              name='name'
              id='name'
              placeholder='Enter your cats name here'
              value={this.state.cat.name}
              onChange={this.handleName}
              invalid={this.state.invalid}
              valid={!this.state.invalid}
            />
            <FormFeedback>You need to give your cat a name.</FormFeedback>
          </FormGroup>
          <FormGroup>
            <Button
              color='info'
              className='d-inline mt-2'
              onClick={this.handleHungry.bind(this, true)}
              outline={!this.state.cat.hungry}
            >
              <FontAwesomeIcon icon={faFrown} />
              My cat is hungry
            </Button>
            <Button
              color='info'
              className='d-inline mt-2'
              onClick={this.handleHungry.bind(this, false)}
              outline={this.state.cat.hungry}
            >
              <FontAwesomeIcon icon={faSmile} />
              My cat is fed
            </Button>
          </FormGroup>
          <FormGroup className='mb-2'>
            <Label for='breakfast'>Breakfast time</Label>
            <Input
              type='time'
              name='breakfast'
              id='breakfast'
              value={this.state.cat.breakfast}
              onChange={this.handleBreakfast}
            />
          </FormGroup>
          <FormGroup className='mb-2'>
            <Label for='dinner'>Dinner time</Label>
            <Input
              type='time'
              name='dinner'
              id='dinner'
              value={this.state.cat.dinner}
              onChange={this.handleDinner}
            />
          </FormGroup>
          <FormGroup>
            <Input
              type='file'
              accept='image/*'
              name='file'
              id='image'
              onChange={this.handleFile}
              hidden={true}
            />
            <Button
              color='secondary'
              className='d-block mt-2'
              onClick={this.handleFileChoose}
            >
              <FontAwesomeIcon icon={faFile} />
              Pick cat photo
            </Button>
            <FormText color='muted'>A nice photo of your kitty.</FormText>
          </FormGroup>
          {this.state.cat.image && (
            <img
              src={this.state.cat.image}
              alt={this.state.cat.name}
              width='300px'
              className='d-block mb-1'
            />
          )}
          <Button
            color='primary'
            disabled={this.state.invalid || this.state.loading}
            onClick={this.handleSubmit}
          >
            <FontAwesomeIcon icon={faSave} />
            Submit
          </Button>
        </Form>
      </div>
    );
  }

  render() {
    return <div>{this.state.loading ? <Spinner /> : this.renderBody()}</div>;
  }
}
