import React, {ChangeEvent} from 'react';
import {CatEdit} from './CatEdit';
import {shallow} from 'enzyme';
import {FileService} from '../FileService';
import {CatService} from './CatService';

describe(CatEdit.name, () => {
  const match: any = {
    params: {
      id: 'new',
    },
  };
  const history: any = {
    push: jest.fn(),
  };
  const location: any = {};
  const wrapper = shallow<CatEdit>(
    <CatEdit match={match} history={history} location={location} />,
  );

  it('should mount an existing cat', () => {
    wrapper.setProps({match: {params: {id: 1}}} as any);
    wrapper.instance().componentDidMount();
  });

  it('should handle hungry', () => {
    const instance = wrapper.instance();
    const cat = {hungry: false, name: ''};
    instance.setState({cat});
    instance.handleHungry(true);
    expect(instance.state.cat.hungry).toBeTruthy();
  });

  it('should handle breakfast', () => {
    const instance = wrapper.instance();
    const cat = {hungry: false, name: '', breakfast: '00:00:00'};
    instance.setState({cat});
    instance.handleBreakfast({target: {value: '01:00:00'}} as any);
    expect(instance.state.cat.breakfast).toEqual('01:00:00');
  });

  it('should handle dinner', () => {
    const instance = wrapper.instance();
    const cat = {hungry: false, name: '', dinner: '00:00:00'};
    instance.setState({cat});
    instance.handleDinner({target: {value: '01:00:00'}} as any);
    expect(instance.state.cat.dinner).toEqual('01:00:00');
  });

  it('should handle name', () => {
    const instance = wrapper.instance();
    const cat = {name: 'old-name', hungry: false};
    instance.setState({cat});
    instance.handleName({target: {value: 'new-name'}} as ChangeEvent<
      HTMLInputElement
    >);
    expect(instance.state.cat.name).toEqual('new-name');
  });

  it('should do nothing with empty file list', () => {
    const instance = wrapper.instance();
    const cat = {image: 'old-image', name: '', hungry: false};
    instance.setState({cat});
    instance.handleFile({target: {files: []}} as any);
    expect(instance.state.cat.image).toEqual('old-image');
  });

  it('should handle file', async () => {
    const instance = wrapper.instance();
    const cat = {image: 'old-image', name: '', hungry: false};
    instance.setState({cat});
    FileService.convert = () => Promise.resolve('new-image');
    const newImage = new File([], '');
    await instance.handleFile({target: {files: [newImage]}} as any);
    expect(instance.state.cat.image).toEqual('new-image');
  });

  it('should create cat', () => {
    const instance = wrapper.instance();
    CatService.create = jest.fn();
    const cat = {name: 'name', hungry: false, image: 'image'};
    instance.setState({cat});
    wrapper.setProps({history: {push: jest.fn()}} as any);
    instance.handleSubmit();
    expect(CatService.create).toHaveBeenCalledWith(cat);
  });

  it('should update cat', () => {
    const instance = wrapper.instance();
    CatService.update = jest.fn();
    const cat = {id: 1, name: 'name', hungry: false, image: 'image'};
    instance.setState({cat});
    wrapper.setProps({
      history: {push: jest.fn()},
    } as any);
    instance.handleSubmit();
    expect(CatService.update).toHaveBeenCalledWith(cat);
  });

  it('should choose file', () => {
    document.getElementById = jest.fn(() => ({click: () => true})) as any;
    const instance = wrapper.instance();
    instance.handleFileChoose();
    expect(document.getElementById).toHaveBeenCalled();
  });

  it('should throw error when image element doesnt exist', () => {
    document.getElementById = jest.fn();
    const instance = wrapper.instance();
    expect(instance.handleFileChoose).toThrowError(
      'Input element with id="image" does not exist.',
    );
  });

  it('should prevent default on form', () => {
    const instance = wrapper.instance();
    const fakePrevent = jest.fn();
    instance.handlePreventDefault({preventDefault: fakePrevent} as any);
    expect(fakePrevent).toHaveBeenCalled();
  });
});
