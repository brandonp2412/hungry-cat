export interface Cat {
  name: string;
  hungry: boolean;
  id?: number;
  image?: string;
  breakfast?: string;
  dinner?: string;
}
