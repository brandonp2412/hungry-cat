import React from 'react';
import {CatTable} from './CatTable';
import {shallow} from 'enzyme';
import {CatService} from './CatService';
jest.mock('reactstrap-confirm', () => () => true);

describe(CatTable.name, () => {
  const cat = {id: 1, name: 'Cat', hungry: false, breakfast: '00:00:00'};
  const wrapper = shallow<CatTable>(
    <CatTable cats={[cat]} onChange={jest.fn} loading={true} />,
  );

  it('can handle remove', async () => {
    const instance = wrapper.instance();
    CatService.remove = jest.fn();
    await instance.handleRemove(cat);
    expect(CatService.remove).toBeCalledWith(cat);
  });

  it('can stop loading', () => {
    wrapper.setProps({loading: false});
  });

  it('should convert 12 hour time of 24', () => {
    expect(wrapper.instance().to12Hour('24:00:00')).toEqual('12:00 AM');
    expect(wrapper.instance().to12Hour('13:00:00')).toEqual('1:00 PM');
  });

  it('should render hungry cats', () => {
    const hungryCat = {...cat, hungry: true};
    wrapper.setProps({cats: [hungryCat]});
  });
});
