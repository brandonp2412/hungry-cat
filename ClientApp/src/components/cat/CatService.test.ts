import {CatService} from './CatService';
import authService from '../api-authorization/AuthorizeService';

describe(CatService.name, () => {
  it('should get', async () => {
    window.fetch = jest.fn(() => ({status: 200, json: () => true} as any));
    authService.getAccessToken = jest.fn();
    await CatService.get();
    expect(authService.getAccessToken).toHaveBeenCalled();
    expect(window.fetch).toHaveBeenCalled();
  });

  it('should navigate to login on 401', async () => {
    window.location.reload = jest.fn();
    window.fetch = () => {
      return Promise.resolve({
        ok: true,
        status: 401,
      } as any);
    };
    authService.getAccessToken = jest.fn();
    await CatService.get();
  });

  it('should update', async () => {
    window.fetch = jest.fn();
    authService.getAccessToken = jest.fn();
    const cat = {id: 1, name: 'Cat', hungry: false};
    await CatService.update(cat);
    expect(authService.getAccessToken).toHaveBeenCalled();
    expect(window.fetch).toHaveBeenCalled();
  });

  it('should remove', async () => {
    window.fetch = jest.fn();
    authService.getAccessToken = jest.fn();
    const cat = {id: 1, name: 'Cat', hungry: false};
    await CatService.remove(cat);
    expect(authService.getAccessToken).toHaveBeenCalled();
    expect(window.fetch).toHaveBeenCalled();
  });

  it('should create', async () => {
    window.fetch = jest.fn();
    authService.getAccessToken = jest.fn();
    const cat = {id: 1, name: 'Cat', hungry: false};
    await CatService.create(cat);
    expect(authService.getAccessToken).toHaveBeenCalled();
    expect(window.fetch).toHaveBeenCalled();
  });
});
