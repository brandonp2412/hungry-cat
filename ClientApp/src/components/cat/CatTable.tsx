import React, {Component} from 'react';
import {Cat} from './Cat.model';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faEdit, faTrash, faShare} from '@fortawesome/free-solid-svg-icons';
import {Button, Spinner} from 'reactstrap';
import {NavLink} from 'react-router-dom';
// @ts-ignore
import confirm from 'reactstrap-confirm';
import {CatService} from './CatService';

export type CatTableProps = {cats: Cat[]; onChange: () => {}; loading: boolean};

export class CatTable extends Component<CatTableProps> {
  constructor(props: CatTableProps) {
    super(props);
    this.handleRemove = this.handleRemove.bind(this);
  }

  async handleRemove(cat: Cat) {
    const confirmed = await confirm({
      title: `Are you sure you want to remove "${cat.name}"?`,
      message: 'Deleting a cat removes it permanently.',
    });
    if (!confirmed) return;
    await CatService.remove(cat);
    this.props.onChange();
  }

  /**
   * Convert 24 hour time into 12 hour time.
   * @param time 24 hour time string. E.g. 17:24
   * @returns 12 hour time string. E.g. 5:24 PM
   */
  to12Hour(time?: string) {
    if (!time) return;
    const hour24 = +time.substr(0, 2);
    const hour12 = hour24 % 12 || 12;
    const amOrPm = hour24 < 12 || hour24 === 24 ? 'AM' : 'PM';
    return hour12 + time.substr(2, 3) + ' ' + amOrPm;
  }

  static spinnerRow() {
    return (
      <tr>
        <td>
          <Spinner />
        </td>
        <td />
        <td />
        <td />
        <td />
      </tr>
    );
  }

  render() {
    return (
      <table className='table table-striped' aria-labelledby='tabelLabel'>
        <thead>
          <tr>
            <th>Name</th>
            <th>Hungry?</th>
            <th>Breakfast</th>
            <th>Dinner</th>
            <th />
          </tr>
        </thead>
        <tbody>
          {this.props.loading
            ? CatTable.spinnerRow()
            : this.props.cats.map((cat) => (
                <tr key={cat.id}>
                  <td>{cat.name}</td>
                  <td>{cat.hungry ? 'Yes' : 'No'}</td>
                  <td>{this.to12Hour(cat.breakfast)}</td>
                  <td>{this.to12Hour(cat.dinner)}</td>
                  <td>
                    <NavLink to={'/cat/' + cat.id}>
                      <Button color='secondary' outline={true}>
                        <FontAwesomeIcon icon={faEdit} />
                        Edit
                      </Button>
                    </NavLink>
                    <NavLink to={'/share-request/' + cat.id}>
                      <Button color='info' outline={true} className='ml-3'>
                        <FontAwesomeIcon icon={faShare} />
                        Share
                      </Button>
                    </NavLink>
                    <Button
                      color='danger'
                      className='ml-3'
                      onClick={this.handleRemove.bind(this, cat)}
                      outline={true}
                    >
                      <FontAwesomeIcon icon={faTrash} />
                      Delete
                    </Button>
                  </td>
                </tr>
              ))}
        </tbody>
      </table>
    );
  }
}
