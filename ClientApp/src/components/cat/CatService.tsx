import authService from '../api-authorization/AuthorizeService';
import {Cat} from './Cat.model';
import {createBrowserHistory} from 'history';
import {ApplicationPaths} from '../api-authorization/ApiAuthorizationConstants';

export class CatService {
  static resourceUrl = 'api/Cats';

  static async get() {
    const token = await authService.getAccessToken();
    const response = await fetch(this.resourceUrl, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    if (response.status === 401) {
      const browserHistory = createBrowserHistory();
      browserHistory.push(ApplicationPaths.Login);
      window.location.reload(false);
      return Promise.resolve([]);
    }
    return await response.json();
  }

  static async find(id: number) {
    const token = await authService.getAccessToken();
    return fetch(this.resourceUrl + `/${id}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  }

  static async update(cat: Cat) {
    const token = await authService.getAccessToken();
    return fetch(this.resourceUrl + `/${cat.id}`, {
      method: 'PUT',
      headers: {
        'Authorization': `Bearer ${token}`,
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(cat),
    });
  }

  static async remove(cat: Cat) {
    const token = await authService.getAccessToken();
    return fetch(this.resourceUrl + `/${cat.id}`, {
      method: 'DELETE',
      headers: {
        'Authorization': `Bearer ${token}`,
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(cat),
    });
  }

  static async create(cat: Cat) {
    const token = await authService.getAccessToken();
    return fetch(this.resourceUrl, {
      method: 'POST',
      headers: {
        'Authorization': `Bearer ${token}`,
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(cat),
    });
  }
}
