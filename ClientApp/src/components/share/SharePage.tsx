import React from 'react';
import {Component} from 'react';
import {ShareService} from './ShareService';
import {ShareTable} from './ShareTable';
import {Share} from './Share.model';
import {Button} from 'reactstrap';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faShare, faSync} from '@fortawesome/free-solid-svg-icons';
import {NavLink} from 'react-router-dom';

export class SharePage extends Component<
  {},
  {
    shares: Share[];
    loading: boolean;
  }
> {
  static displayName = SharePage.name;

  constructor(props: {}) {
    super(props);
    this.state = {
      shares: [],
      loading: false,
    };
    this.getShares = this.getShares.bind(this);
  }

  async componentDidMount() {
    this.getShares();
  }

  async getShares() {
    try {
      this.setState({loading: true});
      this.setState({shares: await ShareService.get()});
    } finally {
      this.setState({loading: false});
    }
  }

  render() {
    return (
      <div>
        <h1 className='d-inline'>Your Cat Requests</h1>
        <div className='float-right'>
          <Button
            className='mr-2'
            outline={true}
            color='info'
            onClick={this.getShares}
          >
            <FontAwesomeIcon icon={faSync} />
          </Button>
          <NavLink className='text-dark' to='/share-request/new'>
            <Button color='primary'>
              <FontAwesomeIcon icon={faShare} />
              Share Cat
            </Button>
          </NavLink>
        </div>
        <p>Here you can manage the sharing of Cats.</p>
        <ShareTable
          onChange={this.getShares}
          shares={this.state.shares}
          loading={this.state.loading}
        />
      </div>
    );
  }
}
