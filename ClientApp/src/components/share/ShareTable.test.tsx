import React from 'react';
import {ShareTable} from './ShareTable';
import {shallow} from 'enzyme';
import {Share} from './Share.model';
import {ApplicationUser} from '../api-authorization/ApplicationUser.model';
import {Cat} from '../cat/Cat.model';

describe(ShareTable.name, () => {
  const shares = [
    {
      catId: 1,
      requestorId: 2,
      requesteeId: 3,
      requestee: {email: 'fake@email.com'} as ApplicationUser,
      requestor: {email: 'requestor@email.com'} as ApplicationUser,
      cat: {name: 'fake cat'} as Cat,
    },
  ] as Share[];
  const wrapper = shallow<ShareTable>(
    <ShareTable shares={shares} onChange={jest.fn()} loading={false} />,
  );

  it('should handle reject', async () => {
    const shareTable = wrapper.instance();
    await shareTable.handleReject(shares[0]);
  });

  it('should handle accept', async () => {
    const shareTable = wrapper.instance();
    await shareTable.handleAccept(shares[0]);
  });

  it('should render spinner', async () => {
    wrapper.setProps({loading: true});
  });
});
