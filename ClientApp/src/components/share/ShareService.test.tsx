import {ShareService} from './ShareService';

describe(ShareService.name, () => {
  it('should create', async () => {
    await ShareService.create(1, 'test@email.com');
  });

  it('should get', async () => {
    await ShareService.get();
    window.location.reload = jest.fn();
    window.fetch = () => {
      return Promise.resolve({
        ok: true,
        status: 401,
      } as any);
    };
    await ShareService.get();
  });

  it('should remove', async () => {
    await ShareService.remove(1);
  });

  it('should accept', async () => {
    await ShareService.accept(1);
  });
});
