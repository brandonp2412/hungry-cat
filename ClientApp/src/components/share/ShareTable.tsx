import React, {Component} from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faCheck, faTimes} from '@fortawesome/free-solid-svg-icons';
import {Button, Spinner} from 'reactstrap';
import {Share} from './Share.model';
import {ShareService} from './ShareService';

export type ShareTableProps = {
  shares: Share[];
  onChange: () => {};
  loading: boolean;
};

export class ShareTable extends Component<ShareTableProps> {
  constructor(props: ShareTableProps) {
    super(props);
    this.handleReject = this.handleReject.bind(this);
  }

  async handleReject(shareCat: Share) {
    await ShareService.remove(shareCat.catId);
    this.props.onChange();
  }

  async handleAccept(shareCat: Share) {
    await ShareService.accept(shareCat.catId);
    this.props.onChange();
  }

  static spinnerRow() {
    return (
      <tr>
        <td>
          <Spinner />
        </td>
        <td />
        <td />
        <td />
        <td />
      </tr>
    );
  }

  render() {
    return (
      <table className='table table-striped' aria-labelledby='tabelLabel'>
        <thead>
          <tr>
            <th>Cat Name</th>
            <th>Requested By</th>
            <th>Requested For</th>
            <th />
          </tr>
        </thead>
        <tbody>
          {this.props.loading
            ? ShareTable.spinnerRow()
            : this.props.shares.map((share) => (
                <tr key={share.catId.toString() + share.requesteeId.toString()}>
                  <td>{share.cat.name}</td>
                  <td>{share.requestor.email}</td>
                  <td>{share.requestee.email}</td>
                  <td>
                    <Button
                      color='danger'
                      className='ml-3'
                      outline={true}
                      onClick={this.handleReject.bind(this, share)}
                    >
                      <FontAwesomeIcon icon={faTimes} />
                      Reject
                    </Button>
                    <Button
                      color='success'
                      className='ml-3'
                      outline={true}
                      onClick={this.handleAccept.bind(this, share)}
                    >
                      <FontAwesomeIcon icon={faCheck} />
                      Accept
                    </Button>
                  </td>
                </tr>
              ))}
        </tbody>
      </table>
    );
  }
}
