import authService from '../api-authorization/AuthorizeService';
import {createBrowserHistory} from 'history';
import {ApplicationPaths} from '../api-authorization/ApiAuthorizationConstants';

export class ShareService {
  static resourceUrl = 'api/ShareCatRequests';

  static async create(_catId: number, _email: string) {
    const token = await authService.getAccessToken();
    return fetch(this.resourceUrl, {
      method: 'POST',
      body: JSON.stringify({
        catId: _catId,
        email: _email,
      }),
      headers: {
        'Authorization': `Bearer ${token}`,
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
    });
  }

  static async get() {
    const token = await authService.getAccessToken();
    const response = await fetch(this.resourceUrl, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    if (response.status === 401) {
      const browserHistory = createBrowserHistory();
      browserHistory.push(ApplicationPaths.Login);
      window.location.reload(false);
      return Promise.resolve([]);
    }
    return await response.json();
  }

  static async remove(catId: number) {
    const token = await authService.getAccessToken();
    return fetch(this.resourceUrl + `/${catId}`, {
      method: 'DELETE',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  }

  static async accept(_catId: number) {
    const token = await authService.getAccessToken();
    return fetch(this.resourceUrl + '/accept', {
      method: 'POST',
      body: JSON.stringify({
        catId: _catId,
      }),
      headers: {
        'Authorization': `Bearer ${token}`,
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
    });
  }
}
