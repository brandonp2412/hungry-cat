import {Cat} from '../cat/Cat.model';
import {ApplicationUser} from '../api-authorization/ApplicationUser.model';

export interface Share {
  catId: number;
  cat: Cat;
  requestorId: number;
  requestor: ApplicationUser;
  requesteeId: number;
  requestee: ApplicationUser;
}
