import React, {Component, ChangeEvent} from 'react';
import {Cat} from '../cat/Cat.model';
import {Button, FormGroup, Label, Input, FormFeedback} from 'reactstrap';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faSave} from '@fortawesome/free-solid-svg-icons';
import {ShareService} from './ShareService';
import {RouteComponentProps} from 'react-router-dom';
import {CatService} from '../cat/CatService';

export type ShareEditProps = RouteComponentProps<{id: string}>;

export class ShareEdit extends Component<
  ShareEditProps,
  {
    cats: Cat[];
    email: string;
    selected: number;
    validEmail: boolean;
    validCat: boolean;
  }
> {
  constructor(props: ShareEditProps) {
    super(props);
    this.state = {
      cats: [],
      email: '',
      selected: 0,
      validEmail: false,
      validCat: false,
    };
    this.handleEmail = this.handleEmail.bind(this);
    this.handleSave = this.handleSave.bind(this);
  }

  async componentDidMount() {
    this.setState({cats: await CatService.get()});
    const id = Number(this.props.match.params.id);
    if (isNaN(id)) return;
    this.setState({selected: id, validCat: !!id});
  }

  validateEmail(email?: string) {
    if (!email) return false;
    const re = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
    return re.test(String(email).toLowerCase());
  }

  handleSelect(id?: number) {
    if (!id) return;
    this.setState({selected: id, validCat: !!id});
  }

  handleEmail(event: ChangeEvent<HTMLInputElement>) {
    this.setState({
      email: event.target.value,
      validEmail: this.validateEmail(event.target.value),
    });
  }

  async handleSave() {
    await ShareService.create(this.state.selected, this.state.email);
    this.props.history.push('/');
  }

  isSelected(id?: number) {
    return this.state.selected === id;
  }

  render() {
    return (
      <div>
        <h1>Share a Cat with your Friend</h1>
        {this.state.cats.map((cat) => (
          <Button
            color='info'
            outline={!this.isSelected(cat.id)}
            key={cat.id}
            onClick={this.handleSelect.bind(this, cat.id)}
          >
            {cat.name}
          </Button>
        ))}
        <FormGroup className='mb-2'>
          <Label for='email'>Email</Label>
          <Input
            type='email'
            name='email'
            id='email'
            placeholder='Enter your friends email here'
            value={this.state.email}
            onChange={this.handleEmail}
            invalid={!this.state.validEmail}
            valid={this.state.validEmail}
          />
          <FormFeedback>You need to specify an email address</FormFeedback>
        </FormGroup>
        <Button
          color='primary'
          disabled={!this.state.validEmail || !this.state.validCat}
          onClick={this.handleSave}
        >
          <FontAwesomeIcon icon={faSave} />
          Save
        </Button>
      </div>
    );
  }
}
