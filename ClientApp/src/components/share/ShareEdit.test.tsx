import {ShareEdit} from './ShareEdit';
import React, {ChangeEvent} from 'react';
import {shallow} from 'enzyme';
import {CatService} from '../cat/CatService';
import {ShareService} from './ShareService';
import {Cat} from '../cat/Cat.model';

describe(ShareEdit.name, () => {
  CatService.get = () =>
    Promise.resolve([{id: 1, name: 'test cat', hungry: false} as Cat]);
  const idParam = '1';
  const wrapper = shallow<ShareEdit>(
    <ShareEdit
      history={{push: jest.fn()} as any}
      match={{params: {id: idParam}} as any}
      location={{} as any}
    />,
  );

  it('should parse id from param', () => {
    const shareEdit = wrapper.instance();
    expect(shareEdit.state.selected).toEqual(Number(idParam));
    const wrapperNewParam = shallow<ShareEdit>(
      <ShareEdit
        history={{push: jest.fn()} as any}
        match={{params: {id: 'new'}} as any}
        location={{} as any}
      />,
    );
    const shareEditNew = wrapperNewParam.instance();
    expect(shareEditNew.state.selected).toEqual(0);
  });

  it('should validate email', () => {
    expect(wrapper.instance().validateEmail('test@email.com')).toBeTruthy();
    expect(wrapper.instance().validateEmail('')).toBeFalsy();
    expect(wrapper.instance().validateEmail('te@')).toBeFalsy();
  });

  it('should handle select', () => {
    const id = 1;
    const shareEdit = wrapper.instance();
    shareEdit.handleSelect(id);
    expect(shareEdit.state.selected).toEqual(id);
    shareEdit.handleSelect();
    expect(shareEdit.state.selected).toEqual(id);
  });

  it('should handle email', () => {
    const email = 'test@email.com';
    const shareEdit = wrapper.instance();
    shareEdit.handleEmail({target: {value: email}} as ChangeEvent<
      HTMLInputElement
    >);
    expect(shareEdit.state.email).toEqual(email);
  });

  it('should handle save', () => {
    const shareEdit = wrapper.instance();
    const _email = 'test@email.com';
    const _selected = 1;
    shareEdit.setState({email: _email, selected: _selected});
    ShareService.create = jest.fn();
    shareEdit.handleSave();
    expect(ShareService.create).toHaveBeenCalledWith(_selected, _email);
  });

  it('should check if id is selected', () => {
    const shareEdit = wrapper.instance();
    const id = 1;
    shareEdit.setState({selected: id});
    expect(shareEdit.isSelected(id)).toBeTruthy();
    expect(shareEdit.isSelected(id + 1)).toBeFalsy();
  });
});
