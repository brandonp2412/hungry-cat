Hungry cat is a website used to manage the hunger status of your cats.

# Installation

- [.NET Core installation](https://dotnet.microsoft.com/download)
- [Node.JS installation](https://nodejs.org/en/download/)
- [Docker installation](https://docs.docker.com/get-docker/) (Optional)

# Running

Run the database migrations:

```
dotnet ef database update
```

Replace the [external login authentication variables](https://docs.microsoft.com/en-us/aspnet/core/security/authentication/social/?view=aspnetcore-3.1&tabs=visual-studio):

```
cp .env.example .env
vim .env
```

Then, you can run the app:

```
dotnet run
```

## Running in Docker

```
docker-compose up
```

## Configuring External Authentication

Third party [external authentication](https://docs.microsoft.com/en-us/aspnet/core/security/authentication/social/?view=aspnetcore-3.1&tabs=visual-studio) is optional, if you set any of the
environment variables shown in `.env.example`, that provider will be enabled.

```
cp .env.example .env
```

# Related Documentation

- [Create React App](https://create-react-app.dev/docs/getting-started/)
- [.NET Core SPA](https://docs.microsoft.com/en-us/aspnet/core/client-side/spa/react?view=aspnetcore-3.1&tabs=visual-studio)

# Credits

- Hungry cat by Royyan Wijaya from the Noun Project
