FROM node:12 AS build-node
WORKDIR /ClientApp
COPY ClientApp/package*.json ./
RUN npm install --loglevel silent
COPY ClientApp .
COPY .prettierrc .
RUN npm run test:ci && \
  npm run lint && \
  npm run prettier && \
  npm run build

FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build-dotnet
WORKDIR /app
COPY *.csproj .
RUN dotnet restore
COPY . .
COPY --from=build-node /ClientApp/build ./ClientApp/build
RUN dotnet publish -c Release -o out
RUN dotnet dev-certs https --clean
RUN dotnet dev-certs https -ep /app/out/hungry-cat.pfx -p "$SSL_PASSWORD"

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1 
WORKDIR /app
COPY --from=build-dotnet /app/out .
ENV ASPNETCORE_Kestrel__Certificates__Default__Path=/app/hungry-cat.pfx
ENV ASPNETCORE_Kestrel__Certificates__Default__Password="$SSL_PASSWORD"
HEALTHCHECK CMD curl --fail localhost:5000/health-check || exit 1
EXPOSE 80 443
ENTRYPOINT ["dotnet", "hungry-cat.dll"]
