using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using hungry_cat.Data;
using hungry_cat.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;

namespace hungry_cat
{
  public class Startup
  {
    public Startup(IConfiguration configuration)
    {
      Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    public void ConfigureServices(
      IServiceCollection services
    )
    {
      DotNetEnv.Env.Load(".env");

      var dbHost = Environment.GetEnvironmentVariable("DB_HOST");
      var dbName = Environment.GetEnvironmentVariable("DB_NAME");
      var dbUser = Environment.GetEnvironmentVariable("DB_USER");
      var dbPassword = Environment.GetEnvironmentVariable("DB_PASSWORD");
      if (dbName == null || dbHost == null || dbUser == null || dbPassword == null)
        throw new Exception(@"
            All database connection variables must be set: 
            DB_NAME,DB_HOST,DB_USER,DB_PASSWORD.
          "
        );
      services.AddDbContext<ApplicationDbContext>(options =>
        options.UseSqlServer(
          $"Server={dbHost};Database={dbName};User id={dbUser};Password={dbPassword}"
        )
      );

      services.AddSwaggerGen(config =>
      {
        config.SwaggerDoc("v3", new OpenApiInfo
        {
          Title = "Hungry Cat",
          Version = "v3"
        });
      });

      services.AddDefaultIdentity<ApplicationUser>(
        options => options.SignIn.RequireConfirmedAccount = false
      )
          .AddEntityFrameworkStores<ApplicationDbContext>();
      services.AddIdentityServer()
          .AddApiAuthorization<ApplicationUser, ApplicationDbContext>();

      var auth = services.AddAuthentication().AddIdentityServerJwt();

      var googleId = Environment.GetEnvironmentVariable("GOOGLE_ID");
      var googleSecret = Environment.GetEnvironmentVariable("GOOGLE_SECRET");
      if (googleId != null && googleSecret != null)
        auth.AddGoogle(options =>
        {
          options.ClientId = googleId;
          options.ClientSecret = googleSecret;
        });

      var facebookId = Environment.GetEnvironmentVariable("FACEBOOK_ID");
      var facebookSecret = Environment.GetEnvironmentVariable("FACEBOOK_SECRET");
      if (facebookId != null && facebookSecret != null)
        auth.AddFacebook(options =>
          {
            options.ClientId = facebookId;
            options.ClientSecret = facebookSecret;
          });

      var microsoftId = Environment.GetEnvironmentVariable("MICROSOFT_ID");
      var microsoftSecret = Environment.GetEnvironmentVariable("MICROSOFT_SECRET");
      if (microsoftId != null && microsoftSecret != null)
        auth.AddMicrosoftAccount(options =>
        {
          options.ClientId = microsoftId;
          options.ClientSecret = microsoftSecret;
        });

      services.AddRazorPages()
        .AddNewtonsoftJson(options =>
        {
          options.SerializerSettings.ReferenceLoopHandling =
            Newtonsoft.Json.ReferenceLoopHandling.Ignore;
        });

      services.AddRazorPages(
        options => options.Conventions.ConfigureFilter(
          new IgnoreAntiforgeryTokenAttribute()
        )
      );

      services.Configure<ForwardedHeadersOptions>(options =>
      {
        options.ForwardedHeaders =
          Microsoft.AspNetCore.HttpOverrides.ForwardedHeaders.XForwardedFor |
            ForwardedHeaders.XForwardedProto;
      });

      services.AddLogging(opt =>
      {
        opt.AddConsole(c =>
        {
          c.TimestampFormat = "[d/M/yyyy hh:mm tt] ";
        });
      });

      services.AddSpaStaticFiles(configuration =>
      {
        configuration.RootPath = "ClientApp/build";
      });

      services.AddHealthChecks();

      services.AddHostedService<MealSchedule>();
    }

    public void Configure(
      IApplicationBuilder app,
      IWebHostEnvironment env,
      ApplicationDbContext dbContext
    )
    {
      if (env.IsDevelopment())
      {
        app.UseDeveloperExceptionPage();
        app.UseDatabaseErrorPage();
      }
      else
      {
        app.UseExceptionHandler("/Error");
        app.UseHsts();
        app.UseForwardedHeaders();
      }

      app.UseSwagger();
      app.UseSwaggerUI(c =>
      {
        c.SwaggerEndpoint("/swagger/v3/swagger.json", "Hungry Cat");
      });

      app.UseStaticFiles();
      app.UseSpaStaticFiles();

      app.UseRouting();

      app.UseAuthentication();
      app.UseIdentityServer();
      app.UseAuthorization();
      app.UseEndpoints(endpoints =>
      {
        endpoints.MapControllerRoute(
                  name: "default",
                  pattern: "{controller}/{action=Index}/{id?}");
        endpoints.MapRazorPages();
        endpoints.MapHealthChecks("/health-check");
      });

      app.UseSpa(spa =>
      {
        spa.Options.SourcePath = "ClientApp";

        if (env.IsDevelopment())
        {
          spa.UseProxyToSpaDevelopmentServer("http://localhost:3000");
        }
      });
    }
  }
}
