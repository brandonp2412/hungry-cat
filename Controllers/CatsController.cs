using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using hungry_cat.Data;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using hungry_cat.Models;
using Microsoft.Extensions.Logging;

namespace hungry_cat.Controllers
{
  [Authorize]
  [Route("api/[controller]")]
  [ApiController]
  public class CatsController : ControllerBase
  {
    private readonly ApplicationDbContext _context;

    private readonly ILogger<CatsController> _logger;

    private string OwnerId
    {
      get => User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
    }

    public CatsController(
      ApplicationDbContext context,
      ILogger<CatsController> logger
    )
    {
      _context = context;
      _logger = logger;
    }

    // GET: api/Cat
    [HttpGet]
    public async Task<List<Cat>> GetCats()
    {
      return await _context.ApplicationUserCat
        .Where(ac => ac.ApplicationUserId == this.OwnerId)
        .Select(ac => ac.Cat)
        .ToListAsync();
    }

    // GET: api/Cat/5
    [HttpGet("{id}")]
    public async Task<ActionResult<Cat>> GetCat(long id)
    {
      var cat = await _context.ApplicationUserCat
        .Where(ac => ac.ApplicationUserId == this.OwnerId)
        .Select(ac => ac.Cat)
        .Where(c => c.Id == id)
        .FirstAsync();

      if (cat == null)
      {
        return NotFound();
      }

      return cat;
    }

    // PUT: api/Cat/5
    // To protect from overposting attacks, enable the specific properties you want to bind to, for
    // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
    [HttpPut("{id}")]
    public async Task<IActionResult> PutCat(long id, Cat cat)
    {
      if (id != cat.Id)
      {
        return BadRequest();
      }

      _context.Entry(cat).State = EntityState.Modified;

      try
      {
        await _context.SaveChangesAsync();
      }
      catch (DbUpdateConcurrencyException)
      {
        if (!CatExists(id))
        {
          return NotFound();
        }
        else
        {
          throw;
        }
      }

      return NoContent();
    }

    // POST: api/Cat
    // To protect from overposting attacks, enable the specific properties you want to bind to, for
    // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
    [HttpPost]
    public async Task<ActionResult<Cat>> PostCat(Cat cat)
    {
      _logger.LogInformation("Creating a Cat...");
      _context.Cats.Add(cat);

      await _context.SaveChangesAsync();
      _logger.LogInformation($"Created a Cat {cat}");

      var ac = new ApplicationUserCat
      {
        CatId = cat.Id,
        ApplicationUserId = this.OwnerId
      };
      _logger.LogInformation($"Associating cat: {ac}");
      _context.ApplicationUserCat.Add(ac);
      await _context.SaveChangesAsync();

      return CreatedAtAction("GetCat", new { id = cat.Id }, cat);
    }

    // DELETE: api/Cat/5
    [HttpDelete("{id}")]
    public async Task<ActionResult<Cat>> DeleteCat(long id)
    {
      var cat = await _context.Cats.FindAsync(id);
      if (cat == null)
      {
        return NotFound();
      }

      _context.Cats.Remove(cat);
      await _context.SaveChangesAsync();

      return cat;
    }

    private bool CatExists(long id)
    {
      return _context.Cats.Any(e => e.Id == id);
    }
  }
}
