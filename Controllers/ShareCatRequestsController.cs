using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using hungry_cat.Data;
using hungry_cat.Models;
using System.Security.Claims;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authorization;

namespace hungry_cat.Controllers
{
  [Route("api/[controller]")]
  [Authorize]
  [ApiController]
  public class ShareCatRequestsController : ControllerBase
  {
    private readonly ApplicationDbContext _context;

    private readonly ILogger<ShareCatRequestsController> _logger;

    private string OwnerId
    {
      get => User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
    }

    public ShareCatRequestsController(
      ApplicationDbContext context,
      ILogger<ShareCatRequestsController> logger
    )
    {
      _context = context;
      _logger = logger;
    }

    // GET: api/ShareCatRequests
    [HttpGet]
    public async Task<ActionResult<IEnumerable<ShareCatRequest>>> GetShareCatRequest()
    {
      return await _context.ShareCatRequests
        .Where(
          sc => sc.RequesteeId == this.OwnerId || sc.RequestorId == this.OwnerId
        )
        .Include(sc => sc.Cat)
        .Include(sc => sc.Requestee)
        .Include(sc => sc.Requestor)
        .ToListAsync();
    }

    // GET: api/ShareCatRequests/5
    [HttpGet("{id}")]
    public async Task<ActionResult<ShareCatRequest>> GetShareCatRequest(
      long id
    )
    {
      var shareCatRequest = await _context.ShareCatRequests
        .Where(sc => sc.RequesteeId == this.OwnerId)
        .Where(sc => sc.CatId == id)
        .Include(sc => sc.Cat)
        .Include(sc => sc.Requestor)
        .FirstAsync();

      if (shareCatRequest == null)
      {
        return NotFound();
      }

      return shareCatRequest;
    }

    // PUT: api/ShareCatRequests/5
    // To protect from overposting attacks, enable the specific properties you want to bind to, for
    // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
    [HttpPut("{id}")]
    public async Task<IActionResult> PutShareCatRequest(
      long id,
      ShareCatRequest shareCatRequest
    )
    {
      if (id != shareCatRequest.CatId)
      {
        return BadRequest();
      }

      shareCatRequest.RequesteeId = this.OwnerId;
      _context.Entry(shareCatRequest).State = EntityState.Modified;

      try
      {
        await _context.SaveChangesAsync();
      }
      catch (DbUpdateConcurrencyException)
      {
        if (!ShareCatRequestExists(id))
        {
          return NotFound();
        }
        else
        {
          throw;
        }
      }

      return NoContent();
    }

    // POST: api/ShareCatRequests
    // To protect from overposting attacks, enable the specific properties you want to bind to, for
    // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
    [HttpPost]
    public async Task<ActionResult<ShareCatRequest>> PostShareCatRequest(
      ShareCatRequestDTO share
    )
    {
      var user = await _context.AspNetUsers.Where(u => u.Email == share.Email)
        .FirstAsync();
      if (user == null)
        return NotFound();
      _logger.LogInformation($"Found user {user.Email}");

      var shareCatRequest = new ShareCatRequest
      {
        RequesteeId = user.Id,
        RequestorId = this.OwnerId,
        CatId = share.CatId
      };
      _context.ShareCatRequests.Add(shareCatRequest);

      try
      {
        await _context.SaveChangesAsync();
        _logger.LogInformation($"Created a ShareCatRequest {share}");
      }
      catch (DbUpdateException)
      {
        if (ShareCatRequestExists(shareCatRequest.CatId))
        {
          return Conflict();
        }
        else
        {
          throw;
        }
      }

      return CreatedAtAction("GetShareCatRequest", new { id = shareCatRequest.CatId }, shareCatRequest);
    }

    // DELETE: api/ShareCatRequests/5
    [HttpDelete("{id}")]
    public async Task<ActionResult<ShareCatRequest>> DeleteShareCatRequest(
      long id
    )
    {
      var shareCatRequest = await _context.ShareCatRequests
        .Where(sc => sc.CatId == id)
        .FirstAsync();
      if (shareCatRequest == null)
      {
        return NotFound();
      }

      _context.ShareCatRequests.Remove(shareCatRequest);
      await _context.SaveChangesAsync();

      return shareCatRequest;
    }

    [HttpPost("accept")]
    public async Task<IActionResult> acceptCatRequest(ShareCatRequestDTO share)
    {
      _logger.LogInformation($"REST request to accept: {share}");
      var sc = await _context.ShareCatRequests
        .Where(sc => sc.CatId == share.CatId)
        .Where(sc => sc.RequesteeId == this.OwnerId)
        .FirstAsync();

      if (sc == null)
      {
        return NotFound();
      }

      _context.Add(new ApplicationUserCat
      {
        ApplicationUserId = this.OwnerId,
        CatId = share.CatId
      });
      _context.ShareCatRequests.Remove(sc);

      try
      {
        await _context.SaveChangesAsync();
        _logger.LogInformation($"Accepted request to share {share.CatId}");
      }
      catch (DbUpdateConcurrencyException)
      {
        if (!ShareCatRequestExists(share.CatId))
        {
          return NotFound();
        }
        else
        {
          throw;
        }
      }

      return NoContent();
    }

    private bool ShareCatRequestExists(long id)
    {
      return _context.ShareCatRequests.Any(e => e.CatId == id);
    }
  }
}
